import 'dart:async';

import 'package:flutter/services.dart';

class Payments {
  static const MethodChannel _channel =
      const MethodChannel('payments');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
